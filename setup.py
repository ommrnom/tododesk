from setuptools import setup

setup(
    name='tododesk',
    packages=['tododesk'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask-bootstrap'
    ],
)
