from flask import Flask

from tododesk.models import Task
from tododesk.forms import TaskForm
# from tododesk.admin import admin


def create_app(name=__name__):
    app = Flask(__name__, instance_relative_config=True)
    load_config(app)

    # Flask Admin
    # admin.init_app(app)

    return app


def load_config(app):
    app.config.from_object(__name__)
    app.config.from_object('config')
    app.config.from_pyfile('config.py', silent=True)
    app.config.from_envvar('TODODESK_SETTINGS', silent=True)

app = create_app(__name__)


