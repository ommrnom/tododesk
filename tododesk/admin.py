from flask import request, jsonify, url_for
from flask_admin import Admin
from flask_admin.contrib.mongoengine import ModelView
from flask_admin.model.template import macro

from tododesk import app
from tododesk.models import Task
from tododesk.task import magic


class TaskView(ModelView):
    # WTF how to extend existing model columns without explicitly redefining all their names?
    column_list = ('title', 'description', 'status', 'magic_computation_status', 'magic_result', 'magic_button')
    column_searchable_list = ['title', 'description']
    column_editable_list = ['title', 'description']
    create_modal = True
    edit_modal = True
    can_export = True
    column_formatters = dict(status=macro('render_status'),
                             magic_computation_status=macro('render_computation_status'),
                             magic_result=macro('render_magic_result'),
                             magic_button=macro('render_magic_button'))
    list_template = '/admin/list.html'


admin = Admin(name='tododesk', template_mode='bootstrap3')
task_view = TaskView(Task)
admin.add_view(task_view)


@app.route('/magic', methods=['POST'])
def make_magic():
    print("make_magic +++")
    if request.method == 'POST':
        id = request.form['id']
        print(id)
        # WTF where to get an object (db or client?)
        task = Task.objects.get(id=id)
        title = task.title
        description = task.description
        celery_task = magic.delay(title, description)
        print("make_magic ---")
        return jsonify({}), 202, {'Location': url_for('magic_status', task_id=celery_task.id)}


@app.route('/magic/<task_id>')
def magic_status(task_id):
    print("magic_status +++")
    task = magic.AsyncResult(task_id)

    if task.state == 'PENDING' or task.state == 'STARTED':
        response = {
            'state': task.state,
            'status': 'in-progress'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'status': 'in-progress'
        }
        if task.get():  # task.info won't work
            response['status'] = 'done'
            response['result'] = task.get()
    else:  # FAILURE
        response = {
            'state': task.state,
            'status': 'not-computed'
        }
    print("magic_status ---")
    return jsonify(response)