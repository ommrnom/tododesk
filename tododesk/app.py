from flask import jsonify, render_template, request, url_for
from flask_bootstrap import Bootstrap
from flask_basicauth import BasicAuth
from flask_mongoengine import MongoEngine
from flask_wtf.csrf import CSRFProtect

from tododesk import app
from tododesk.admin import admin
from tododesk.models import Task
from tododesk.forms import TaskForm
# from tododesk.task import magic

# Flask Extensions
basic_auth = BasicAuth(app)
Bootstrap(app)
MongoEngine(app)
CSRFProtect(app)
admin.init_app(app)


@app.route('/')
# @basic_auth.required
def desk():
    form = TaskForm()
    return render_template('desk.html', tasks=Task.objects, statuses=Task.STATUSES, form=form)


@app.route('/task', methods=['POST'])
def add_task():
    form = TaskForm(request.form)
    if form.validate_on_submit():
        task = Task(title=form.title.data, description=form.description.data).save()
        return jsonify(id=str(task.id), title=task.title, description=task.description)
    return jsonify(form.title.errors)


@app.route('/task', methods=['PUT'])
def edit_task():
    form = TaskForm(request.form)
    if form.validate_on_submit():
        task = Task.objects.get(id=request.form['id'])  # because objects.get() is not QuerySet
        task.title = form.title.data
        task.description = form.description.data
        task.save()  # because update() uses cache and does not update fields while I want to return updated object >_<
        return jsonify(id=str(task.id), title=task.title, description=task.description)
    return jsonify(form.title.errors)


@app.route('/task', methods=['DELETE'])
def remove_task():
    task_id = request.form['id']
    Task.objects.get(id=task_id).delete()
    return jsonify(id=task_id)


@app.route('/move', methods=['PUT'])
def move_task():
    task_id = request.form['id']
    status = request.form['status']
    task = Task.objects.get(id=task_id)
    if task.status != status:
        task.status = status
        task.save()
    return jsonify(id=task_id, status=task.status)


# @app.route('/magic', methods=['POST'])
# def make_magic():
#     print("make_magic +++")
#     if request.method == 'POST':
#         id = request.form['id']
#         print(id)
#         # WTF where to get an object (db or client?)
#         task = Task.objects.get(id=id)
#         title = task.title
#         description = task.description
#         celery_task = magic.delay(title, description)
#         print("make_magic ---")
#         return jsonify({}), 202, {'Location': url_for('magic_status', task_id=celery_task.id)}
#
#
# @app.route('/magic/<task_id>')
# def magic_status(task_id):
#     print("magic_status +++")
#     task = magic.AsyncResult(task_id)
#
#     if task.state == 'PENDING' or task.state == 'STARTED':
#         response = {
#             'state': task.state,
#             'status': 'in-progress'
#         }
#     elif task.state != 'FAILURE':
#         response = {
#             'state': task.state,
#             'status': 'in-progress'
#         }
#         if task.get():  # task.info won't work
#             response['status'] = 'done'
#             response['result'] = task.get()
#     else:  # FAILURE
#         response = {
#             'state': task.state,
#             'status': 'not-computed'
#         }
#     print("magic_status ---")
#     return jsonify(response)


if __name__ == '__main__':
    app.run()
