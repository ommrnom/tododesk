from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, validators


class TaskForm(FlaskForm):
    title = StringField('Title', validators=[validators.DataRequired(message="Title cannot be empty.")])
    description = TextAreaField('Description')

    class Meta:
        def bind_field(self, form, unbound_field, options):
            filters = unbound_field.kwargs.get('filters', None) or []
            filters.append(strip_filter)
            return unbound_field.bind(form=form, filters=filters, **options)


def strip_filter(value):
    if value is not None and hasattr(value, 'strip'):
        return value.strip()
    return value
