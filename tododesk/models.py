from mongoengine import *
from datetime import datetime


class Task(Document):
    TODO, INPROGRESS, DONE = (('to-do', 'To Do'),
                              ('in-progress', 'In Progress'),
                              ('done', 'Done'))
    STATUSES = (TODO, INPROGRESS, DONE)

    COMPUTATION_STATUSES = (('not-computed', 'Not Computed'),
                            INPROGRESS,
                            DONE)

    title = StringField(max_length=120, required=True)
    description = StringField()
    status = StringField(choices=STATUSES, default='to-do')
    created = DateTimeField(default=datetime.now())
    magic_computation_status = StringField(choices=COMPUTATION_STATUSES, default='not-computed')
    magic_result = IntField()
    meta = {
        'ordering': ['-created']
    }

    def __str__(self):
        return "{} {} {} {}".format(self.id, self.title, self.description, self.status)


def test_task():
    connect('tododbtest', host='mongomock://localhost')
    print(Task(title="Task", description="Description of task", status='done').get_status_display())
    Task(title="Task 1", description="Description of task 1", status='in-progress').save()
    Task(title="Task 2", description="Description of task 2", status='done').save()
    Task(title="Task 3", description="Description of task 3").save()

    for t in Task.objects:
        print(t.__str__())  # Called explicitly because of mongomock


if __name__ == '__main__':
    test_task()
