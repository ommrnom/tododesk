function update_progress(status_url, status_div, result_div) {
    // send GET request to status URL
    $.getJSON(status_url, function (data) {
        status_div.text(data['status']);
        if (data['state'] != 'PENDING' && data['state'] != 'STARTED') {
            if ('result' in data) {
                result_div.text(data.result.result);
                console.log(result_div.text())
            }
            else {
                status_div.text(data['state']);
            }
        }
        else {
            setTimeout(function () {
                update_progress(status_url, status_div, result_div);
            }, 1000);
        }
    });
}

(function () {
    $('.makeMagic').on('click', function (e) {
        e.preventDefault();
        console.log("MAKE MAGIC");
        var id = $(this).attr('data-pk');
        var url = '/magic';
        var xhr = $.ajax({
                type: 'POST',
                url: url,
                data: {id: id},
                success: function () {
                    var status_url = xhr.getResponseHeader('Location');
                    var status_div = $('.magicStatus[data-pk=' + id + ']');
                    var result_div = $('.magicResult[data-pk=' + id + ']');
                    update_progress(status_url, status_div, result_div);
                },
                error: function (error) {
                    console.log(error);
                }
            }
        );
    });
})();
