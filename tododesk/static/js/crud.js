function errorMessage(code) {
    alert("Sorry for alert but " + code + " error occured.")
}

function createTask() {
    var url = "/task";
    $.ajax({
        type: "POST",
        url: url,
        data: $('form').serialize(),
        success: function (response) {
            var id = response.id;
            var title = response.title;
            var description = response.description;
            $("#to-do .sortable").prepend('<div id="' + id + '" class="row task"><div class="col-md-12"><h4>' + title + '</h4><p>' + description + '</p></div></div>');
            $('#taskModal').modal('hide');
        },
        error: function (error) {
            console.log(error);
        },
        // statusCode: {
        //     400: errorMessage(400),
        //     500: errorMessage(500)
        // }
    });
}

function editTask(id) {
    var url = "/task";
    $.ajax({
        type: "PUT",
        url: url,
        data: $('form').serialize() + "&id=" + id, // mda...
        success: function (response) {
            var id = response.id;
            var title = response.title;
            var description = response.description;
            var task = $('#' + id);
            task.find('h4').text(title);
            task.find('p').text(description);
            $('#taskModal').modal('hide');
        },
        error: function (error) {
            console.log(error);
        },
        // statusCode: {
        //     400: errorMessage(400),
        //     500: errorMessage(500)
        // }
    });
}

function removeTask(id) {
    var res = confirm("Delete this task?");
    if (res) {
        var url = "/task";
        $.ajax({
            type: "DELETE",
            url: url,
            data: {id: id},
            success: function (response) {
                $('#' + response.id).remove();
                $('#taskModal').modal('hide');
            },
            error: function (error) {
                console.log(error);
            },
        //     statusCode: {
        //         400: errorMessage(400),
        //         500: errorMessage(500)
        // }
        });
    }
}

function moveTask(task) {
    var id = task.attr('id');
    var url = '/move';
    var status = task.parent().parent().attr('id');
    $.ajax({
        type: "PUT",
        url: url,
        data: {
            id: id,
            status: status},
        success: function (response) {
            console.log(response.id, response.status);
        }
    })
}