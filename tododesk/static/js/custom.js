(function () {

    $('.sortable').sortable({
        connectWith: '.sortable',
        cursor: 'move',
        opacity: 0.9,
        placeholder: 'ui-state-highlight',
        scroll: false,
        stop: function (event, ui) {
            moveTask(ui.item);
        }
    });

    $('#createTask').click(function () {
        $('.modal-title').text('Create New Task');
        $('#inputTitle').val('');
        $('#inputDescription').val('');
        $('#saveTask').attr('value', 'Create');
        $('#deleteTask').hide();

    });

    $('.task').click(function () {
        $('#taskForm').attr('task-id', $(this).attr('id'));
        $('.modal-title').text('Edit Task');
        $('#inputTitle').val($(this).find('h4').text());
        $('#inputDescription').val($(this).find('p').text());
        $('#saveTask').attr('value', 'Edit');
        $('#deleteTask').show();
    });

    $('form').submit(function (e) {
        if ($('#saveTask').attr('value') == 'Create') { // create
            createTask();
            e.preventDefault();
        }
        else { // update
            var id = $('#taskForm').attr('task-id');
            editTask(id);
            e.preventDefault();
        }
    });

    $('#deleteTask').on('click', function () { // delete
        var id = $('#taskForm').attr('task-id');
        removeTask(id);
    });

})();