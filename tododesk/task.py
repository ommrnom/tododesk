from celery import Celery
import nltk
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords

nltk.download('stopwords')

CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
_celery = Celery(backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL)


def count_unique(title, description):
    tokenizer = RegexpTokenizer(r'\w+')
    words = tokenizer.tokenize(title) + tokenizer.tokenize(description)
    unique = set(w.lower() for w in words if w.lower() not in stopwords.words())
    return len(unique)


@_celery.task(bind=True)
def magic(self, title, description):
    print("magic +++")
    self.update_state(state='PENDING')
    self.update_state(state='STARTED')
    result = count_unique(title, description)
    self.update_state(state='COMPLETE')
    print("magic ---")
    return {'status': 'done', 'result': result}
